require('dotenv').config()

const express = require('express')
const cors = require('cors')
const routes = require('./routes')
const DB = require('./db')

const bcrypt = require('bcryptjs')
console.log(bcrypt)
// const mongoose = require('mongoose')

// const mongoLink = 'mongodb+srv://aleksandr:cfif1112@cluster0.dro8k.mongodb.net/<dbname>?retryWrites=true&w=majority'

// mongoose.connect(mongoLink, { useNewUrlParser: true })

// const db = mongoose.connection
// db.once('open', () => console.log('mongo connected'))

// connection.query('SELECT * FROM EVENTS', function (error, results, fields) {
//     if (error) throw error;
//     console.log(results);
// });


// connection.query('INSERT ')

const db = new DB()
db.init()

// console.log(db.getAllEvents().then(result => console.log(result)))

const trans = () => {
    const d = new Date(); 
    return d.toISOString().split('T')[0]+' '+d.toTimeString().split(' ')[0];
}

const values = [
    {
        hid: 1,
        lid: 2
    }
]

// values.forEach(value => {
//     const { hid, lid } = value
//     const query = `
//         INSERT INTO HHasL(hid, lid)
//         VALUES (${hid}, ${lid});
//     `
//     // const query = `
//     //     DELETE FROM DHasA
//     //     WHERE id = 13;
//     // `
//     db.connection.query(query, function (error, results, fields) {
//         if (error) throw error;
//         console.log(results)
//         console.log(fields)
//     })
// })

const app = express()
app.use(express.json())
app.use(cors())
app.use(routes)

const posts = [
    {
        username: 'Kyle',
        title: 'Post 1'
    },
    {
        username: 'John',
        title: 'Post 2'
    }
]

// app.get('/posts', authToken, (req, res) => {
//     res.json(posts.filter(post => post.username === req.user.name))
// })

app.listen(80)