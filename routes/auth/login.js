const router = require('express').Router()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const DB = require('../../db')

const db = new DB()

router.post('/medical', async (req, res) => {
    const userParams = {
        email: req.body.email,
        password: req.body.password
    }
    console.log(userParams)
    db.getMedical(userParams.email)
    .then(async user => {
        if (user.length === 0) return res.send(404).send('Can\'t find user with such email')
        try {
            if (await bcrypt.compare(userParams.password, user[0].password)) {
                console.log('right password')
                const accessToken = jwt.sign(userParams, process.env.ACCESS_TOKEN_SECRET_MEDICAL)
                return res.json({ 
                    status: 200,
                    accessToken
                })
            } else {
                console.log('wrong password')
                return res.json({
                    status: 403
                })
            }
        } catch {
            console.log('error')
            return res.sendStatus(500)
        }
    })
})

router.post('/', async (req, res) => {
    const userParams = {
        email: req.body.email,
        password: req.body.password
    }
    console.log(userParams)
    db.getUser(userParams.email)
    .then(async user => {
        if (user.length === 0) return res.send(404).send('Can\'t find user with such email')
        try {
            if (await bcrypt.compare(userParams.password, user[0].password)) {
                console.log('right password')
                const accessToken = jwt.sign(userParams, process.env.ACCESS_TOKEN_SECRET)
                return res.json({ 
                    status: 200,
                    accessToken
                })
            } else {
                console.log('wrong password')
                return res.json({
                    status: 403
                })
            }
        } catch {
            console.log('error')
            return res.sendStatus(500)
        }
    })
})

module.exports = router;