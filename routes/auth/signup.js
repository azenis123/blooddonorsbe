const router = require('express').Router()
const bcrypt = require('bcryptjs')

const DB = require('../../db')

const db = new DB()

router.post('/', async (req, res) => {
    try {
        const hashPassword = await bcrypt.hash(req.body.password, 10)
        console.log(hashPassword)
        const user = {
            name: req.body.name,
            surname: req.body.surname,
            age: req.body.age,
            type: req.body.bloodType,
            factor: req.body.factor,
            email: req.body.email,
            password: hashPassword,
        }
        db.addUser(user) 
        .then(() => res.send({ status: 'OK' }))
        .catch(err => console.error(err))
    } catch {
        res.sendStatus(500)
    }
})

router.post('/medical', async (req, res) => {
    try {
        const hashPassword = await bcrypt.hash(req.body.password, 10)
        console.log(hashPassword)
        const user = {
            name: req.body.name,
            surname: req.body.surname,
            age: req.body.age,
            email: req.body.email,
            password: hashPassword,
            jobposition: req.body.jobposition
        }
        db.addMedical(user)
        .then(() => res.send({ status: 'OK' }))
        .catch(err => console.error(err))
    } catch {
        res.sendStatus(500)
    }
})

module.exports = router