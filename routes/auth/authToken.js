const jwt = require('jsonwebtoken')
const DB = require('../../db')

const db = new DB()

module.exports = {
    authToken: function (req, res, next) {
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]

        if (token === null) return res.sendStatus(401)

        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
            if (err) return res.sendStatus(403)
            db.getUser(user.email)
            .then(existingUser => {
                if (existingUser.length === 0) return res.sendStatus(403)
                req.user = existingUser[0]
                next()
            })
        })
    },
    authTokenMedical: function (req, res, next) {
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]
    
        if (token === null) return res.sendStatus(401)
    
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET_MEDICAL, (err, user) => {
            if (err) return res.sendStatus(403)
            db.getMedical(user.email)
            .then(existingUser => {
                if (existingUser.length === 0) return res.sendStatus(403)
                req.user = existingUser[0]
                next()
            })
        })
    }
}