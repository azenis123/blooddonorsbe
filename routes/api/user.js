const router = require('express').Router()
const { authToken } = require('../auth/authToken')
const DB = require('../../db')

const db = new DB()

router.get('/', authToken, async (req, res) => {
    const user = await db.getUserInfo(req.user.id)
    res.json(user[0])
})

router.post('/editinfo', authToken, async (req, res) => {
    const id = req.user.id
    const changes = req.body.changes
    const notNullChanges = Object.keys(changes).filter(change => changes[change])
    const filteredChanges = notNullChanges.reduce((acc, change) => {
        return {
            ...acc, 
            [change]: changes[change]
        }
    }, {})

    db.editUserInfo(filteredChanges, id)
    .then(() => res.send({ status: 200 }))
    .catch(() => res.send({ status: 500 }))
    // res.json(user[0])
})

module.exports = router;