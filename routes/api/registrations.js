const router = require('express').Router()
const { authToken, authTokenMedical} = require('../auth/authToken')
const DB = require('../../db')

const db = new DB()

// router.get('/', authToken, async (req, res) => {
router.get('/', authToken, async (req, res) => {
    const id = req.user.id
    const registrations = await db.getRegistrations(id)
    res.json(registrations)
})

router.get('/regshospital', authTokenMedical, async (req, res) => {
    if (!req.user) return res.sendStatus(403)
    // const mid = req.user.id
    const mid = 2
    const registrations = await db.getRegistrationsByHospital(mid)
    res.json(registrations)
})

router.post('/unreghospital', authTokenMedical, async (req, res) => {
    const id = req.body.id 
    console.log("______")
    console.log(id)
    console.log("______")
    console.log("here2")
    db.removeRegistrationByMd(id)
    .then(() => res.json({ status: 200 }))
    .catch(() => {
        console.log("here3")
        res.json({ status: 500 })
    })
})

module.exports = router;