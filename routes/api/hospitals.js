const router = require('express').Router()
const { authToken, authTokenMedical } = require('../auth/authToken')
// const Event = require('../../models/event')
const DB = require('../../db')

const db = new DB()

// router.get('/', authToken, async (req, res) => {
router.get('/', async (req, res) => {
    const hospitals = await db.getAllHospitals()
    res.json(hospitals)
})

router.post('/visitreg', authToken, async (req, res) => {
    const id = req.user.id
    const hid = req.query.id
    const date = req.body.date
    db.addRegistration(id, hid, date)
    .then(() => res.json({ status: 200 }))
    .catch(() => res.json({ status: 500 }))
})

router.get('/bloodneed', async (req, res) => {
    const hid = req.query.id
    db.getBloodneed(hid)
    .then(result => res.json(result))
    .catch(() => res.json({ status: 500 }))
})

router.get('/bloodneedmd', authTokenMedical, async (req, res) => {
    // const id = req.user.id
    const id = 2
    console.log(id)
    db.getHospitalByMid(id)
    .then(hid => db.getBloodneed(hid))
    .then(result => res.json(result))
    .catch(() => res.json({ status: 500 }))
})

router.post('/editbloodneed', authTokenMedical, async (req, res) => {
    // const id = req.user.id
    const id = 2
    const change = req.body.change
    db.getHospitalByMid(id)
    .then(hid => db.editBloodNeed(hid, change))
    .then(() => res.json({ status: 200 }))
    .catch(() => res.json({ status: 500 }))
})

router.post('/visitunreg', authToken, async (req, res) => {
    const id = req.user.id
    const hid = req.query.id
    console.log(id, hid)
    db.removeRegistration(id, hid)
    .then(() => res.json({ status: 200 }))
    .catch(() => res.json({ status: 500 }))
})

router.get('/hospital', async (req, res) => {
    const id = req.query.id;
    const hospital = await db.getHospital(id)
    if (!hospital) return res.sendStatus(404)

    res.json(hospital)
})

module.exports = router;