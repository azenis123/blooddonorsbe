const router = require('express').Router()
const { authToken, authTokenMedical } = require('../auth/authToken')
const DB = require('../../db')

const db = new DB()

router.get('/', authToken, async (req, res) => {
    const achievements = await db.getAchievements(req.user.id)
    res.json(achievements)
})

router.post('/adddonation', authTokenMedical, async (req, res) => {
    const id = req.body.id
    const rid = req.body.rid
    db.addDonation(id)
    .then(() => res.json({ status: 200 }))
    .catch(() => res.json({ status: 500 }))
    db.removeRegistrationByMd(rid)
    .catch(() => {
        res.json({ status: 500 })
    })
    
    db.getDonation(id)
    .then(donations => {
        if (donations.length === 1) {
            db.addAchievement(id, 1)
        } else if (donations.length === 3) {
            db.addAchievement(id, 2)
        } else if (donations.length === 5) {
            db.addAchievement(id, 3)
        }
    })
})

router.get('/all', async (req, res) => {
    const achievements = await db.getAllAchievements()
    res.json(achievements)
})

module.exports = router;