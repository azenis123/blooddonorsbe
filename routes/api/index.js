const router = require('express').Router();

router.use('/events', require('./events'));
router.use('/hospitals', require('./hospitals'));
router.use('/registrations', require('./registrations'));
router.use('/achievements', require('./achievements'));
router.use('/user', require('./user'));

module.exports = router;