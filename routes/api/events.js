const router = require('express').Router()
const { authToken, authTokenMedical } = require('../auth/authToken')
// const Event = require('../../models/event')
const DB = require('../../db')

const db = new DB()

// router.get('/', authToken, async (req, res) => {
router.get('/', async (req, res) => {
    const events = await db.getAllEvents()
    res.json(events)
})

router.get('/event', async (req, res) => {
    const id = req.query.id;
    console.log(id)
    const event = await db.getEvent(id)
    console.log(event)
    if (!event) return res.sendStatus(404)

    res.json(event[0])
})

router.get('/eventsbymd', authTokenMedical, async (req, res) => {
    const id = req.user.id;
    const events = await db.getEventsByMd(id)
    res.json(events)
})

router.post('/cancel', authTokenMedical, async (req, res) => {
    const mid = req.user.id
    if (!mid) res.json({ status: 403 })
    const id = req.query.id
    console.log(id)
    db.deleteEvent(mid, id)
    .then(() => res.json({ status: 200 }))
    .catch(() => res.json({ status: 500 }))
})

router.post('/create', authTokenMedical, async (req, res) => {
    const id = req.user.id
    const event = req.body.event
    console.log(event)
    db.addEvents(id, event)
    .then(() => res.json({ status: 200 }))
    .catch(() => res.json({ status: 500 }))
})

router.post('/subscribe', authToken, async (req, res) => {
    const user = req.user
    const id = req.query.id;
    console.log(user.id, id)
    db.addSubscription(user.id, id)
    .then(() => res.json({ status: 200 }))
    .catch(() => res.json({ status: 500 }))
})

router.post('/unsubscribe', authToken, async (req, res) => {
    const user = req.user
    const id = req.query.id;
    db.removeSubscription(id)
    .then(() => res.json({ status: 200 }))
    .catch(() => res.json({ status: 500 }))
})

router.get('/topdonors', async (req, res) => {
    db.getTopDonors()
    .then(result => res.json(result))
    .catch(() => res.json({ status: 500 }))
})

module.exports = router;