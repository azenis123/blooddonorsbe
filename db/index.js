const mysql = require('mysql')
const getAllEvents = require('./queries/getAllEvents')
// const main = require('../emailService/emailService')
class DB {
    connection = null
    constructor(){
        if (!DB.instance){
            DB.instance = this
            this.connection = mysql.createConnection({
                host: 'localhost',
                user: 'root',
                password: 'password',
                database: 'systems3'
            })
        }
        return DB.instance;
    }

    init() {
        this.connection.connect()
    }

    removeRegistrationByMd(id) {
        console.log("here5")
        return new Promise((resolve, reject) => {
            console.log("here")
            const query = `
                DELETE FROM REGISTRATION
                WHERE id = ${id}
            `
            console.log(query)
            this.connection.query(query, function (error, results, fields) {
                if (error) {
                    console.log(error)
                    reject(error);
                }
                resolve(results)
            })
        })
    }

    getBloodneed(hid) {
        return new Promise((resolve, reject) => {
            const query = `
                SELECT BloodType.name
                FROM BloodNeed, BloodType
                WHERE 
                    BloodNeed.hid = ${hid} AND
                    BloodNeed.bid = BloodType.id
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) {
                    console.log(error)
                    reject(error);
                }
                resolve(results)
            })
        })
    }

    addDonation(did) {
        return new Promise((resolve, reject) => {
            const date = new Date().toString()
            const query = `
                INSERT INTO DONATION(did, date)
                VALUES (${did}, "${date}")
            `
            // const query = `
            //     INSERT INTO DHasA(did, aid, dateOfObrain)
            //     VALUES (${did}, ${aid}, "${dateOfObrain}")
            // `
            this.connection.query(query, function (error, results, fields) {
                if (error) {
                    console.log(error)
                    reject(error);
                }
                resolve(results)
            })
        })
    }
    
    addAchievement(did, aid) {
        return new Promise((resolve, reject) => {
            const date = new Date().toString()
            const query = `
                INSERT INTO DHasA(did, aid, dateOfObrain)
                VALUES (${did}, ${aid}, "${dateOfObrain}")
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) {
                    console.log(error)
                    reject(error);
                }
                resolve(results)
            })
        })
    }
  
    getDonation(did) {
        return new Promise((resolve, reject) => { 
            const query = `
                SELECT * FROM DONATION
                WHERE id = ${did}
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) {
                    console.log(error)
                    reject(error);
                }
                resolve(results)
            })
        })
    }

    getAllAchievements() {
        return new Promise((resolve, reject) => {
            const query = `
                SELECT * FROM ACHIEVEMENTS
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) reject(error);
                resolve(results)
            })
        })
    }

    getAllEvents() {
        console.log('getting events')
        return new Promise((resolve, reject) => {
            const query = `
                SELECT EVENTS.id, EVENTS.realDate, EVENTS.title, EVENTS.description, address, lon, lat
                FROM LOCATION, EHasL, EVENTS
                WHERE EVENTS.id = EHasL.eid AND LOCATION.id = EHasL.lid;
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) reject(error);
                resolve(results)
            })
        })
    }

    editBloodNeed(hid, changes) {
        return new Promise((resolve, reject) => {
            let queryInsert = 'INSERT INTO BloodNeed(hid, bid) VALUES'
            let first = true
            Object.keys(changes).forEach((change, i) => {
                if (changes[change]) {
                    queryInsert += `${first ? ' ' : ', '}(${hid}, ${i + 1})`
                    if (first) first = false
                }
            })
            
            const query = `
                DELETE FROM BloodNeed
                WHERE hid = ${hid};
            `
            console.log('lol1')
            this.connection.query(query, (error, results, fields) => {
                if (error) {
                    console.log(error)
                    reject(error)
                }
                console.log(queryInsert)
                this.connection.query(queryInsert, function (error, results, fields) {
                    if (error) {
                        console.log(error)
                        reject(error)
                    }
                    resolve()
                })
            })
        })
    }

    getHospitalByMid(mid) {
        return new Promise((resolve, reject) => {
            console.log('mid is ' + mid)
            const query = `
                SELECT hid 
                FROM WORKS
                WHERE mid = ${mid}
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) {
                    console.log(error)
                    reject(error)
                }
                console.log("________")
                console.log(results)
                resolve(results[0].hid)
            })
        })
    }

    getRegistrationsByHospital(mid) {
        return new Promise((resolve, reject) => {
            this.getHospitalByMid(mid)
            .then(hid => {
                const query = `
                    SELECT DONOR.id AS did, DONOR.name, DONOR.surname, DONOR.email, REGISTRATION.date, REGISTRATION.id AS id
                    FROM DONOR, REGISTRATION
                    WHERE REGISTRATION.hid = ${hid} AND REGISTRATION.did = DONOR.id
                `
                this.connection.query(query, function (error, results, fields) {
                    if (error) reject(error);
                    resolve(results)
                })
            })
        })
    }

    getEvent(id) {
        return new Promise((resolve, reject) => {
            const query = `
                SELECT EVENTS.id, EVENTS.realDate, EVENTS.title, EVENTS.description, address, lon, lat
                FROM LOCATION, EHasL, EVENTS
                WHERE EVENTS.id = ${id} AND EVENTS.id = EHasL.eid AND LOCATION.id = EHasL.lid;
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) reject(error);
                resolve(results)
            })
        })
    }

    // becomeLocalDonor(did, hid) {
    //     return new Promise((resolve, reject) => {
    //         const query = `
    //             INSERT INTO Subcription(did, eid)
    //             VALUES (${did}, ${eid});
    //         `
    //         this.connection.query(query, function (error, results, fields) {
    //             if (error) reject(error);
    //             resolve(results)
    //         })
    //     })
    // }

    getAllHospitals() {
        console.log('getting hospitals')
        return new Promise((resolve, reject) => {
            const query = `
                SELECT HOSPITAL.id, HOSPITAL.name, HOSPITAL.phone, LOCATION.address, LOCATION.lon, LOCATION.lat
                FROM LOCATION, HHasL, HOSPITAL
                WHERE HOSPITAL.id = HHasL.hid AND LOCATION.id = HHasL.lid;
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) reject(error);
                resolve(results)
            })
        })
    }

    deleteEvent(mid, id) {
        return new Promise((resolve, reject) => {
            const query = `
                DELETE FROM EVENTS
                WHERE id = ${id}
            `
            this.connection.query(query, function (error, results, fields) {
                // main()
                console.log(error)
                if (error) reject(error);
                resolve(results)
            })
        })
    }

    getHospital(id) {
        return new Promise((resolve, reject) => {
            const query = `
                SELECT HOSPITAL.id, HOSPITAL.name, HOSPITAL.phone, address, lon, lat
                FROM LOCATION, HHasL, HOSPITAL
                WHERE HOSPITAL.id = ${id} AND HOSPITAL.id = HHasL.hid AND LOCATION.id = HHasL.lid;
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) reject(error);
                resolve(results[0])
            })
        })
    }

    async getUser(email) {
        console.log(email)
        return new Promise((resolve, reject) => {
            const query = `
                SELECT * FROM DONOR
                WHERE email = "${email}"
            `
            this.connection.query(query, (error, results, fields) => {
                if (error) reject(error)
                resolve(results)
            })
        })
    }
    
    async getMedical(email) {
        console.log(email)
        return new Promise((resolve, reject) => {
            const query = `
                SELECT * FROM MedicalWorker
                WHERE email = "${email}"
            `
            this.connection.query(query, (error, results, fields) => {
                if (error) reject(error)
                resolve(results)
            })
        })
    }

    async getUserInfo(id) {
        return new Promise((resolve, reject) => {
            const query = `
                SELECT name, surname, age, email FROM DONOR
                WHERE id = "${id}"
            `
            this.connection.query(query, (error, results, fields) => {
                if (error) reject(error)
                resolve(results)
            })
        })
    }

    async editUserInfo(changes, id) {
        return new Promise((resolve, reject) => { 
            const changesStr = Object.keys(changes).reduce((acc, change, i) => {
                const isNumber = typeof changes[change] === 'number'
                const newVal = `${isNumber ? '' : '"'}${changes[change]}${isNumber ? '' : '"'}`
                return acc + `${i !== 0 ? ', ' : ''}${change} = ${newVal}`
            }, '')
            const query = `
                UPDATE DONOR
                SET ${changesStr}
                WHERE DONOR.id = ${id}; 
            `
            this.connection.query(query, (error, results, fields) => {
                if (error) {
                    console.log(error)
                    reject(error)
                }
                resolve(results)
            })
        })

    }

    async getTopDonors() {
        return new Promise((resolve, reject) => {
            const query = `
                SELECT DONOR.name, DONOR.surname, COUNT(*) AS QUANTITY
                FROM DONOR, DHasA
                WHERE DONOR.id = DHasA.did
                GROUP BY DONOR.id
                ORDER BY QUANTITY DESC
                LIMIT 10;
            `
            this.connection.query(query, (error, results, fields) => {
                if (error) {
                    console.log(error)
                    reject(error)
                }
                resolve(results)
            })
        })
    }

    async addRegistration(id, hid, date) {
        console.log(id, hid, date)
        return new Promise((resolve, reject) => {
            const query = `
                INSERT INTO REGISTRATION(did, hid, date)
                VALUES (${id}, ${hid}, "${date}");
            `
            this.connection.query(query, (error, results, fields) => {
                if (error) reject(error)
                resolve(results)
            })
        })
    }

    async removeRegistration(id, hid){
        return new Promise((resolve, reject) => {
            console.log(id, hid)
            const query = `
                DELETE FROM REGISTRATION
                WHERE did = ${id} AND hid = ${hid};
            `
            this.connection.query(query, (error, results, fields) => {
                console.log(error)
                if (error) reject(error)
                resolve(results)
            })
        })
    }

    async getRegistrations(id) {
        return new Promise((resolve, reject) => {
            const query = `
                SELECT EVENTS.id AS eid, EVENTS.title, EVENTS.realDate, LOCATION.address, Subcription.id AS sid
                FROM EVENTS, Subcription, LOCATION, EHasL
                WHERE 
                    Subcription.did=${id} AND
                    Subcription.eid = EVENTS.id AND
                    EHasL.eid = EVENTS.id AND
                    EHasL.lid = LOCATION.id;
            `
            this.connection.query(query, (error, results, fields) => {
                if (error) reject(error)
                const events = results
                const query = `
                    SELECT HOSPITAL.id AS hid, HOSPITAL.name, LOCATION.address, LOCATION.lat, LOCATION.lon, REGISTRATION.date, REGISTRATION.id as rid
                    FROM HOSPITAL, REGISTRATION, LOCATION, HHasL
                    WHERE 
                        REGISTRATION.did=${id} AND
                        REGISTRATION.hid = HOSPITAL.id AND
                        HHasL.hid = HOSPITAL.id AND
                        HHasL.lid = LOCATION.id;
                `
                this.connection.query(query, (error, results, fields) => {
                    if (error) reject(error)
                    resolve({
                        events,
                        hospitals: results
                    })
                })
            })
        })
    }

    checkSubscription(did, eid) {
        console.log('here?')
        return new Promise((resolve, reject) => {
            const query = `
                SELECT *
                FROM Subcription
                WHERE eid = ${eid} AND did = ${did}
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) reject(error);
                resolve(results.length > 0)
            })
        })
    } 

    registerVisit(id, date) {

    }

    addSubscription(did, eid) {
        return new Promise((resolve, reject) => {
            this.checkSubscription(did, eid)
            .then(exist => {
                if (exist) return resolve()
                console.log("exist")
                console.log(exist)
                const query = `
                    INSERT INTO Subcription(did, eid)
                    VALUES (${did}, ${eid});
                `
                this.connection.query(query, function (error, results, fields) {
                    if (error) reject(error);
                    resolve(results)
                })
            })
        })
    }

    removeSubscription(id) {
        return new Promise((resolve, reject) => {
            console.log(id)
            const query = `
                DELETE FROM Subcription
                WHERE Subcription.id = ${id}
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) {
                    console.log(error)
                    reject(error);
                }
                resolve(results)
            })
        })
    }

    getAchievements(id) {
        return new Promise((resolve, reject) => {
            const query = `
                SELECT *
                FROM ACHIEVEMENT, DONOR, DHasA
                WHERE
                    DONOR.id = ${id} AND
                    DONOR.id = DHasA.did AND
                    ACHIEVEMENT.id = DHasA.aid;
            `
            console.log(query)
            this.connection.query(query, function (error, results, fields) {
                if (error) {
                    console.log(error)
                    reject(error);
                }
                resolve(results)
            })
        })
    }

    addUser(newUser) {
        return new Promise((resolve, reject) => {
            this.getUser(newUser.email)
            .then(user => {
                if (user.length !== 0) return reject({ status: 'Already exist' })

                const { name, surname, age, email, password, type, factor } = newUser
                const query = `
                    INSERT INTO DONOR(name, surname, age, email, password)
                    VALUES ("${name}", "${surname}", ${age}, "${email}", "${password}");
                `
                this.connection.query(query, (error, results, fields) => {
                    if (error) reject(error)
                    const map = {
                        '1+': 1,
                        '1-': 2,
                        '2+': 3,
                        '2-': 4,
                        '3+': 5,
                        '3-': 6,
                        '4+': 7,
                        '4-': 8
                    }
                    const newRecord = `${results.insertId}, ${map[type + factor]}`
                    const query = `
                        INSERT INTO DHasBT(did, bid)
                        VALUES (${newRecord});
                    `
                    this.connection.query(query, function (error, results, fields) {
                        if (error) reject(error)
                        resolve(results)
                    })
                })
            })
            .catch(err => console.log(err))
        })
    }

    addEvents(id, event) {
        return new Promise((resolve, reject) => {
            const { title, description, realDate, lat, lon, address } = event
            const query = `
                INSERT INTO EVENTS(title, description, realDate)
                VALUES ("${title}", "${description}", "${realDate}");
            `
            this.connection.query(query, (error, results, fields) => {
                if (error) {
                    console.log(error)
                    reject(error);
                }
                const eid = results.insertId
                const query = `
                    INSERT INTO Orginises(mid, eid)
                    VALUES (${id}, ${eid});
                `
                this.connection.query(query, (error, results, fields) => {
                    if (error) {
                        console.log(error)
                        reject(error);
                    }
                    const query = `
                        SELECT id FROM LOCATION
                        WHERE lat = ${lat} AND lon = ${lon};
                    `
                    this.connection.query(query, (error, results, fields) => {
                        if (error) {
                            console.log(error)
                            reject(error);
                        }
                        let lid
                        if (results.length === 0) {
                            const query = `
                                INSERT INTO LOCATION(address, lat, lon)
                                VALUES ("${address}", ${lat}, ${lon});
                            `
                            this.connection.query(query, (error, results, fields) => {
                                if (error) {
                                    console.log(error)
                                    reject(error);
                                }
                                lid = results.insertId
                                const query = `
                                    INSERT INTO EHasL(eid, lid)
                                    VALUES (${eid}, ${lid});
                                `
                                this.connection.query(query, (error, results, fields) => {
                                    if (error) {
                                        console.log(error)
                                        reject(error);
                                    }
                                    resolve()
                                })
                            })
                        } else {
                            lid = results[0].id
                            const query = `
                                INSERT INTO EHasL(eid, lid)
                                VALUES (${eid}, ${lid});
                            `
                            this.connection.query(query, (error, results, fields) => {
                                if (error) {
                                    console.log(error)
                                    reject(error);
                                }
                                resolve()
                            })
                        }
                    })
                })
            })
        })
    }

    getEventsByMd(id) {
        return new Promise((resolve, reject) => {
            const query = `
                SELECT EVENTS.id, EVENTS.title, EVENTS.realDate, EVENTS.description
                FROM MedicalWorker, Orginises, EVENTS
                WHERE
                    MedicalWorker.id = ${id} AND
                    Orginises.mid = MedicalWorker.id AND
                    Orginises.eid = EVENTS.id;
            `
            this.connection.query(query, function (error, results, fields) {
                if (error) {
                    console.log(error)
                    reject(error);
                }
                resolve(results)
            })
        })
    }

    addMedical(newUser) {
        return new Promise((resolve, reject) => {
            this.getUser(newUser.email)
            .then(user => {
                if (user.length !== 0) return reject({ status: 'Already exist' })
                const { name, surname, age, email, password, jobposition } = newUser
                const query = `
                    INSERT INTO MedicalWorker(name, surname, age, email, password, jobposition)
                    VALUES ("${name}", "${surname}", ${age}, "${email}", "${password}", "${jobposition}");
                `
                this.connection.query(query, (error, results, fields) => {
                    if (error) reject(error)
                    resolve(results)
                
                    // const map = {
                    //     '1+': 1,
                    //     '1-': 2,
                    //     '2+': 3,
                    //     '2-': 4,
                    //     '3+': 5,
                    //     '3-': 6,
                    //     '4+': 7,
                    //     '4-': 8
                    // }
                    // const newRecord = `${results.insertId}, ${map[type + factor]}`
                    // const query = `
                    //     INSERT INTO MHasBT(did, bid)
                    //     VALUES (${newRecord});
                    // `
                    // this.connection.query(query, function (error, results, fields) {
                    //     if (error) reject(error)
                    //     resolve(results)
                    // })
                })
            })
            .catch(err => console.log(err))
        })
    }
}

module.exports = DB